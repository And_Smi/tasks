// function for output first upper letter of word
const upperFirst = (word) => {
    return word[0].toUpperCase();
};

// function for output a word without first letter
const withoutFirst = (word) => {
    return word.substr(1);
};

// function for output a replaced word in string
const myReplace = (str, before, after) => {

    const isUpperBefore = before[0] === before[0].toUpperCase();

    if(isUpperBefore) {
        after = upperFirst(after) + withoutFirst(after);
    }

    str = str.replace(before, after);

    return str;
};

// data for functions
console.log(myReplace('Let us go to the store', 'store', 'mall'));
console.log(myReplace('He is Sleeping on the couch','Sleeping','sitting'));
console.log(myReplace('This has a spellngi error','spellngi','spelling'));
console.log(myReplace('His name is Tom', 'Tom', 'john'));
console.log(myReplace('Let us get back to more Coding','Coding','algorithms'));