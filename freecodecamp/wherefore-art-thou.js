/* 
  Сделать функцию, которая ищет пару ключ-значение в массиве объектов и возвращает найденные объекты  в массив
*/

function whatIsInAName(collection, source) {  
  var arr = [];
  var keysInSource = Object.keys(source);
  var valsInSource = Object.values(source);
  
  console.log(collection);
  console.log(source);

  arr = collection.filter(obj => 
    keysInSource.every(property => obj.hasOwnProperty(property)) && 
    valsInSource.every(value => Object.values(obj).includes(value)));

  console.log(arr);
  return arr;
}

whatIsInAName([{ "apple": 1, "bat": 2 }, { "bat": 2 }, { "apple": 1, "bat": 2, "cookie": 2 }], { "apple": 1, "bat": 2 });
