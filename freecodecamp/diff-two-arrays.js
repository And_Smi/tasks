/* 
  Сделать функцию, которая определяет, в чем разница одного массива от другого, в результате получается массив с уникальными значениями
*/

function diffArray(arr1, arr2) {
  var newArr = [];
  
 newArr = arr1
    .filter(x => !arr2.includes(x))
    .concat(arr2.filter(x => !arr1.includes(x)));

  console.log(newArr);
  return newArr;
}

diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]);
