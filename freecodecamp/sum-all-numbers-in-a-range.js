/* 
  Сделать функцию, которая возвращает сумму всех значений от минимального (указанного в массиве) до максимального (указанного в массиве) в формате целочисленного числа. 
*/

function sumAll(arr) {
  var maxVal = Math.max.apply(null, arr);
  var minVal = Math.min.apply(null, arr);
  var result = 0;

  for(var i = minVal; i <= maxVal; i++) {
    result += i;
  }

  console.log(result);
  return result;
}

sumAll([1, 4]);
