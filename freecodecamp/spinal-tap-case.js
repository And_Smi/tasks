const spinalCase = str => {
    console.log('Normal input: '+str)
    str = str.replace(/(_|\s)/g, '-').replace(/(\w)([A-Z])/g, '$1-$2');
    console.log("First input: "+str);
    str = str.toLowerCase();
    return str;
};

spinalCase('thisIsSpinalTap');
